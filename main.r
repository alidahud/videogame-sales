library(tidyverse)
library(plotly)

vgsales2020 <- read.csv("output/vgsales2020.csv")
vgsales <- read.csv("input/vgsales.csv")
vgsales <- vgsales %>% mutate(Rating=gsub("($^)",NA,Rating))


#filtering out the video games through 2016 till 2018... here the observations of critic score and user score are like 30 or 40 compared to 500
#in the vgsales dataset downloaded from kaggle so I will be using that more through my project.
vg2016_8 <- vgsales2020 %>% filter(Release_Date>=2016&Release_Date<=2018)

# Filtering out the 2016 video games
vg2016 <- vgsales %>%
  filter(Year == 2016)
vg2016 <- vg2016 %>% mutate(Platform=factor(vg2016$Platform))

#Creating the annual vgsales table
annual_vgsales <- vgsales2020 %>%
  mutate(across(.cols = ends_with("Sales"),.fns = ~as.numeric(replace(.,is.na(.x),0)))) %>% 
  group_by(Release_Date) %>% 
  filter(!is.na(Release_Date),Release_Date!= 1970,!is.na(Total_Sales)) 

annual_vgsales_sum <- annual_vgsales %>% 
  summarise(Total=sum(Total_Sales,na.rm = T)) 

annual_vgsales_sum_by_group <- annual_vgsales %>% 
    summarise(`Na Sales`=sum(NA_Sales),
            `Pal Sales`=sum(PAL_Sales),
            `Jp Sales`=sum(Japan_Sales),
            `Other Sales`=sum(Other_Sales))
  
annual_vgsales_cumsum <- annual_vgsales_sum_by_group %>%
    group_by(Release_Date) %>% 
  summarise(`Na Sales`=cumsum(`Na Sales`),
            `Pal Sales`=cumsum(`Pal Sales`),
            `Jp Sales`=cumsum(`Jp Sales`),
            `Other Sales`=cumsum(`Other Sales`))

# Creating a scatter plot of User_Score against Critic_Score
# scatterplots-- between two numerical variables
#adding a linear smoother to the scatterplot of user score against critic score for video games in through 2016 till 2018.
# Fit the regression model of User_Score on Critic_Score to use it as a way of showing the mean of their estimated values.
m <- lm(User_Score ~ Critic_Score, data = vg2016)
vg2016 %>% 
  select(User_Score, Critic_Score, Name,Platform) %>%
  na.omit() %>%
  plot_ly(x = ~Critic_Score, y =~User_Score,
          hoverinfo="text",
          text=~paste("User score:",User_Score,"<br>",
                      "Critic score:", Critic_Score,"<br>",
                      "",Name,"<br>",
                      "Platform",Platform) ) %>%
  add_markers(color=~Platform  ) %>% 
  layout(xaxis=list(title="Critic score"),
         yaxis=list(title="User score")) %>%
  # Creating the scatterplot with smoother
  add_lines(y = ~fitted(m)) 





# Creating a faceted scatterplot of User_Score vs. Critic_Score with 3 rows to be able to distnguish platforms
vg2016 %>%
  group_by(Platform) %>%
  filter(!is.na(Critic_Score)&!is.na(User_Score)) %>% 
  do(
    plot = plot_ly(data = ., x=~Critic_Score, y=~User_Score) %>%
      add_markers(name = ~Platform)
  ) %>%
  subplot(nrows = 3, shareY = TRUE, shareX = TRUE) %>% 
  layout(xaxis=list(title="Critic score"),
         yaxis=list(title="User score")) 



#Are best-selling video games generally well received by critics?
# Polishing the scatterplot by transforming the x-axis and labeling both axes
vg2016 %>%
  select(Global_Sales,Critic_Score,Name) %>% 
  na.omit() %>% 
  plot_ly(x = ~Global_Sales, y = ~Critic_Score,text=~Name) %>%
  add_markers(marker = list(opacity = 0.5)) %>%
  layout(xaxis = list(title="Global sales (millions of units)",type="log"),
         yaxis = list(title="Crritic score"))
#generally yes they are well received


#comparing the distribution of ratings between Genres -- plotting proportions
# E (Everyone) · E10+ (Everyone 10+) · T (Teen) · M (Mature 17+) EC (Eqrly childhood) K-A (Kids to adults) AO (Adults only)
# stacked bar charts-- between two categorical variables
vgsales %>%
  
  select(Genre,Rating) %>% 
  filter(Rating!="RP") %>% 
  na.omit() %>% 
  #counts that how many ratingss are there inside a genre
  count(Genre,Rating) %>% 
  #groups it by genre so it calculates proportions only for the groups (genres)
  group_by(Genre) %>% 
  mutate(Proportion=n/sum(n))  %>% 
  plot_ly(x =~Genre, y = ~Proportion, color = ~Rating) %>%
  add_bars() %>%
  layout(barmode = "stack")
#as we might've epected racing games, puzzles, sports are mostly for everyone, and fighting and shooter games are teen and up  


vgsales %>% filter(Rating=="AO")
#GTA: San Andreas is the only AO game which is kind of weird


# Creating a frequency table for Genre
vgsales %>%
  count(Genre) %>% 
#comparing the count of games published by genre (are the action games published more than the others?)
# bar charts are for comparing one or more categorical variable
# Reordering the bars for Genre by count
  mutate(Genre = fct_reorder(Genre, n, .desc = T)) %>%
  plot_ly(x = ~Genre, y = ~n) %>% 
  add_bars() %>% 
  layout(yaxis=list(title="Count"))

#we can see that action games are published more than the others

# boxplots are one way to explore how the distribution of a numerical variable may change based on the level of a cat. var.
# Creating boxplots of Global_Sales by Genre for above data
vg2016 %>% 
  plot_ly (x=~Global_Sales, y=~Genre)  %>%
  add_boxplot()

#We can see that the shooter games are generally more well received by audience in 2016 than oher genres



# How the number of users helps explain the
# relationship between user and critic scores for video games in 2016. 
# Additionally, how applying the natural log can help make a color scale more interpretable.

vg2016 %>%
  mutate(User_Count=log(User_Count)) %>% 
  plot_ly(x=~Critic_Score,y=~User_Score,color=~User_Count)%>%
  add_markers(colors="Dark2")




# How does the market perform over the years?
annual_vgsales_sum %>%
  plot_ly(x = ~Release_Date, y = ~Total) %>%
  add_lines() %>%
  layout(xaxis=list(showgrid=F,range=c(1980,2018)),yaxis=list(gridcolor="black"),paper_bgcolor= "#ebebeb",plot_bgcolor="white" )

annual_vgsales_cumsum %>% 
  plot_ly( x = ~Release_Date, y = ~`Na Sales`, name = 'Na Sales', type = 'scatter', mode = 'none', stackgroup = 'one', fillcolor = '#F5FF8D') %>%
  add_trace(y = ~`Pal Sales`, name= 'PAL Sales', fillcolor = '#4C74C9') %>%
  add_trace(y = ~`Jp Sales`,name = 'Japan Sales', fillcolor = '#700961') %>%
  add_trace(y = ~`Other Sales`, name = 'Other sales', fillcolor = '#312F44') %>% 
  layout(title = 'Sales by Region',
         xaxis = list(title = "Year",
                      showgrid = FALSE,range=c(1980,2018)),
         yaxis = list(title = "",
                      showgrid = FALSE))


#here we can see a downtrend from 2008 because of the introduction of the first smartphones e.g. iPhone-2007


#here we can see the top publishers, thats why I  chose the top three for the density plot
vgsales2020 %>% group_by(Publisher) %>% summarise(Total=sum(Total_Sales,na.rm = T)) %>% arrange(desc(Total))


#creating density plots and overlay them to compare the distribution 
#of critic scores received for three video game publishers: Activision, Electronic Arts, and Nintendo.
#It shows us the distribution of critic score of the games published by these publishers it's like  a histogram its just smoothed out

activision <- vgsales2020 %>% filter(Publisher=="Activision")
ea <- vgsales2020 %>% filter(Publisher=="Electronic Arts")
nintendo <- vgsales2020 %>% filter(Publisher=="Nintendo")
# Compute density curves
d.a <- density( activision$Critic_Score, na.rm = TRUE)
d.e <- density( ea$Critic_Score, na.rm = TRUE)
d.n <- density( nintendo$Critic_Score, na.rm = TRUE)

# Overlay density plots
plot_ly() %>%
  add_lines(x = ~d.a$x, y = ~d.a$y, name = "Activision", fill = 'tozeroy') %>%
  add_lines(x = ~d.e$x, y = ~d.e$y, name = "Electronic Arts", fill = 'tozeroy') %>%
  add_lines(x = ~d.n$x, y = ~d.n$y, name = "Nintendo", fill = 'tozeroy') %>%
  layout(xaxis = list(title = 'Critic Score',showgrid=F),
         yaxis = list(title = 'Density',gridcolor="black"))
#here we can see that nintendo's received critic scores are the closest to normal distribution

#to spot outliers
nintendo %>% arrange(desc(Total_Sales))%>% slice_head(n=50)
activision %>% arrange(desc(Total_Sales))%>% slice_head(n=30)
ea %>% arrange(desc(Total_Sales)) %>% slice_head(n=30)

#filtering them out even more so we don't have to work with small or too big observations
activision <- vgsales2020 %>% filter(Publisher=="Activision",Total_Sales>=0.75,Total_Sales<5)
ea <- vgsales2020 %>% filter(Publisher=="Electronic Arts",Total_Sales>=0.75,Total_Sales<5)
nintendo <- vgsales2020 %>% filter(Publisher=="Nintendo",Total_Sales>=0.75,Total_Sales<5)
d.a <- density( activision$Total_Sales, na.rm = TRUE)
d.e <- density( ea$Total_Sales, na.rm = TRUE)
d.n <- density( nintendo$Total_Sales, na.rm = TRUE)

plot_ly() %>%
  add_lines(x = ~d.a$x, y = ~d.a$y, name = "Activision", fill = 'tozeroy') %>%
  add_lines(x = ~d.e$x, y = ~d.e$y, name = "Electronic Arts", fill = 'tozeroy') %>%
  add_lines(x = ~d.n$x, y = ~d.n$y, name = "Nintendo", fill = 'tozeroy') %>%
  layout(xaxis = list(title = 'Total Sales',showgrid=F,range=c(0)),
         yaxis = list(title = 'Density',gridcolor="black"))

#we can see that the three big game pubishers total sales are close to normal distribution if we dont count the outliers, but ea's is the most normal

# Creating a binned scatterplot of User_Score vs. Critic_Score to see more of this large dataset
vgsales %>%
  plot_ly(x=~Critic_Score, y=~User_Score) %>%
  add_histogram2d(nbinsx=100,nbinsy=100 )

#clustering the NA sales vs PAL sales filtering out the outliers and the too small (irrelevant pieces)
vgsaleskmeans1 <- vgsales2020 %>%
  select(!starts_with("Total")&ends_with(c("A_Sales","L_Sales"))) %>%
  filter(NA_Sales>0.5&NA_Sales<5&PAL_Sales>0.5&PAL_Sales<5)

cl <- kmeans(vgsaleskmeans1,5)

vgsaleskmeans2 <- vgsales2020 %>%
  select(!starts_with("Total")&ends_with(c("rm","me","A_Sales","L_Sales"))) %>%
  filter(NA_Sales>0.5&NA_Sales<5&PAL_Sales>0.5&PAL_Sales<5)

cluster <- factor(paste("Cluster ",cl$cluster))
 
vgsaleskmeans2 %>% 
  plot_ly() %>% 
  add_markers(x=~NA_Sales,y=~PAL_Sales,color=~cluster,hoverinfo = "text",
              text = ~paste("NA_Sales:", NA_Sales, "<br>",
                            "PAL_Sales:", PAL_Sales, "<br>",
                            "Name:",Name, "<br>",
                            "Platform: ", Platform
                            )) %>% 
  layout(xaxis=list(showgrid=F,title="Na sales (millions of units)"),
         yaxis=list(gridcolor="black",title="PAl sales (millions of units)")) %>% 
  add_markers(data = as.data.frame(cl$centers),x=~NA_Sales,y=~PAL_Sales,color=I("black"),name="Centers")

